import Vue from 'vue'
import Router from 'vue-router'
import LoginReg from '@/components/logReg'
import Login from '@/components/auth/login'
import Register from '@/components/auth/register'
import Init1 from '@/components/init/init.1'
import Upload from '@/components/init/uploadbeat/upload'
import Myprofile from '@/components/init/buttons/myprofile/myprofile'
import EditPlaylist from '@/components/init/buttons/editplaylist/editplaylist'

Vue.use(Router)

export default new Router({
  routes: [{
    path: '/',
    name: 'loginReg',
    component: LoginReg,
    meta: {
      forVisitors: true
    }
  },
  {
    path: '/login',
    name: 'login',
    component: Login,
    meta: {
      forVisitors: true
    }
  },
  {
    path: '/register',
    name: 'register',
    component: Register,
    meta: {
      forVisitors: true
    }
  },
  {
    path: '/myprofile',
    name: 'myprofile',
    component: Myprofile,
    meta: {
      forAuth: true
    }
  },
  {
    path: '/editplaylist',
    name: 'editplaylist',
    component: EditPlaylist,
    meta: {
      forAuth: true
    }
  },
  {
    path: '/init1',
    name: 'init1',
    component: Init1,
    meta: {
      forVisitors: true
    }
  },
    {
    path: '/upload',
    name: 'upload',
    component: Upload,
    meta: {
      forAuth: true
    }
  }
  ],

  linkActiveClass: 'active',
  mode: 'history'
})
