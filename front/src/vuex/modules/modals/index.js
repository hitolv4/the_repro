import actions from './actions'
import getters from './getters'
import mutations from './mutations'

const state = {
  checkTermsCond: false,
  spinnerTermsCond: false
}

export default {
  state,
  getters,
  mutations,
  actions
}
