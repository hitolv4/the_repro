const actions = {
  checkTermsCond ({commit}, value) {
    commit('checkTermsCond', value)
  },
  setspinnerTC ({commit}, value) {
    commit('setspinnerTC', value)
  }
}

export default actions
