const getters = {
  checkTermsCond (state) {
    return state.checkTermsCond
  },
  spinnerTermsCond (state) {
    return state.spinnerTermsCond
  }
}

export default getters
