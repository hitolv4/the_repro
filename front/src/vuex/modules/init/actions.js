
const actions = {
  getMoods ({commit}, moods) {
    commit('getMoods', moods)
  },
  getGenre ({commit}, genres) {
    commit('getGenre', genres)
  },

  getMp3 ({commit}, mp3){
  	commit('getMp3', mp3)
},
  getTitle ({commit}, title){
  	commit('getTitle', title)
  }
}

export default actions
