const getters = {
  AllMoods (state) {
    return state.Moods
  },
  AllGenres (state) {
    return state.Genres
  },

  GetMp3 (state){
  	return state.Mp3
  },

  OnlyTitle (state) {
  	return state.Title
  }
}

export default getters
