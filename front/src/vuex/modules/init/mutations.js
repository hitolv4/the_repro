const mutations = {
  getMoods (state, moods) {
    state.Moods = moods
  },
  getGenre (state, genres) {
    state.Genres = genres
  },

  getMp3 (state, mp3){
  	state.Mp3 = mp3
},
  getTitle (state, title) {
  	state.Title = title;
  }
}

export default mutations
