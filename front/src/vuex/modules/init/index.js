import actions from './actions'
import getters from './getters'
import mutations from './mutations'

const state = {
  Moods: [],
  Genres: [],
  Mp3:'',
  Title: ''
}

export default {
  state,
  getters,
  mutations,
  actions
}
