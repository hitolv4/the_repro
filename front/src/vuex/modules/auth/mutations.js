
import Vue from 'vue'
import axios from 'axios'
import VueAxios from 'vue-axios'
Vue.use(VueAxios, axios)
const mutations = {

  isAuthenticated (state) {
    state.isAuthtenticated = true
  },
  logoutAuthenticatedUser (state) {
    state.isAuthtenticated = null
  },
  SetUserType (state, type) {
    state.AuthUserType = type
  },
  SetUserId (state, id) {
    state.AuthId = id
  }

}

export default mutations
