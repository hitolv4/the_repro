
const actions = {

  IsAuthenticated ({commit}) {
    let token = localStorage.token
    if (token) {
      commit('isAuthenticated')
    }
  },
  logoutAuthenticatedUser ({ commit }) {
    commit('logoutAuthenticatedUser')
  },
  SetUserType ({ commit }, type) {
    commit('SetUserType', type)
  },
  SetUserId ({ commit }, id) {
    commit('SetUserId', id)
  }
}

export default actions
