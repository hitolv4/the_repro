const getters = {

  isAuthtenticated (state) {
    return state.isAuthtenticated
  },
  getUser (state) {
    return state.AuthenticatedUser
  },
  AuthUserType (state) {
    return state.AuthUserType
  },
  AuthUserId (state) {
    return state.AuthId
  }
}

export default getters
