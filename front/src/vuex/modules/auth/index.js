import actions from './actions'
import getters from './getters'
import mutations from './mutations'

const state = {

  isAuthtenticated: false,
  AuthUserType: '',
  AuthId: '',

}

export default {
  state,
  getters,
  mutations,
  actions
}
