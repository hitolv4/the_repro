import Vue from 'vue'
import Vuex from 'vuex'
import authModule from './modules/auth'
import modalsModule from './modules/modals'
import initModule from './modules/init'

Vue.use(Vuex)

export const store = new Vuex.Store({

  modules: {
    auth: authModule,
    modals: modalsModule,
    init: initModule
  }

})
