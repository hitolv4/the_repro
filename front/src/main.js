// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import axios from 'axios'
import VueAxios from 'vue-axios'
import Auth from './packages/auth/auth.js'
import {store} from './vuex/store'
import VeeValidate from 'vee-validate'
import './assets/scss/style.scss'
import './assets/css/style.css'
import 'bulma/css/bulma.css'

import './assets/fonts/fira-sans/FiraSans-Book.otf'
import './assets/fonts/fira-sans/FiraSans-Bold.otf'
import './assets/fonts/fira-sans/FiraSans-Regular.otf'
import './assets/fonts/fira-sans/FiraSans-Ultra.otf'
/* eslint-disable no-new */
Vue.use(VeeValidate)
Vue.use(VueAxios, axios)
Vue.config.productionTip = false
Vue.use(Auth)

axios.defaults.baseURL = 'http://127.0.0.1:8000'
axios.defaults.headers.common['Authorization'] = 'token ' + Vue.auth.getToken()

router.beforeEach(
  (to, from, next) => {
    if (to.matched.some(record => record.meta.forVisitor)) {
      if (Vue.auth.isAuthenticated()) {
        next({
          path: '/init'
        })
      } else next()
    } else if (to.matched.some(record => record.meta.forAuth)) {
      if (!Vue.auth.isAuthenticated()) {
        next({
          path: '/login'
        })
      } else next()
    } else next()
  }
)
/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  axios,
  store,
  components: {
    App
  },
  template: '<App/>'
})
