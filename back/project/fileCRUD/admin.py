from django.contrib import admin
from .models import Genere, Mood, File
# Register your models here.

admin.site.register(Mood)
admin.site.register(Genere)
admin.site.register(File)
