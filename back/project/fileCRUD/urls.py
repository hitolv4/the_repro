from django.conf.urls import url, include

from rest_framework.routers import DefaultRouter

from .views import GenereViewSet, MoodViewSet, FileViewSet, FileReadViewSet

router = DefaultRouter()
router.register('genere', GenereViewSet, base_name='genere')
router.register('mood', MoodViewSet, base_name='mood')
router.register('beat', FileViewSet, base_name='beat')
router.register('getbeat', FileReadViewSet, base_name='getbeat')

urlpatterns = [
    url(r'', include(router.urls)),
]
