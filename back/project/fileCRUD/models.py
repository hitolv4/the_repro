from django.db import models
from django.conf import settings
from django.db.models import Q


# Create your models here.


class Mood(models.Model):
    title = models.CharField(max_length=100, unique=True)
    description = models.CharField(max_length=250)

    def __str__(self):
        """get object to string"""
        return self.title


class Genere(models.Model):
    title = models.CharField(max_length=100, unique=True)
    description = models.CharField(max_length=250)

    def __str__(self):
        """get object to string"""
        return self.title


class File(models.Model):
    user = models.ForeignKey(settings.AUTH_USER_MODEL,
                             on_delete=models.CASCADE,
                             limit_choices_to=(Q(is_producer=True) |
                                               Q(is_admin=True)))
    title = models.CharField(max_length=100, unique=True)
    duration = models.CharField(max_length=6)
    bpm = models.CharField(max_length=100)
    mood = models.ForeignKey(Mood, on_delete=models.CASCADE)
    genere = models.ForeignKey(Genere, on_delete=models.CASCADE)
    sample_file = models.FileField(upload_to='files/fileCRUD/sample',
                                   blank=False)
    wav = models.FileField(upload_to='files/fileCRUD/wavs', blank=False)
