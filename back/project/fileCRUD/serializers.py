from rest_framework import serializers

from .models import File, Mood, Genere


class MoodSerializer(serializers.ModelSerializer):
    """Serializer for Mood"""
    class Meta:
        model = Mood
        fields = ('id', 'title', 'description')

    def create(self, validated_data):
        mood = Mood(
            title=validated_data['title'],
            description=validated_data['description']
        )
        mood.save()
        return mood


class GenereSerializer(serializers.ModelSerializer):
    """Serializer for Genere"""
    class Meta:
        model = Genere
        fields = ('id', 'title', 'description')

    def create(self, validated_data):
        genere = Genere(
            title=validated_data['title'],
            description=validated_data['description']
        )
        genere.save()
        return genere


class FileSerializer(serializers.ModelSerializer):
    """Serializer for File"""
    class Meta:
        model = File
        fields = ('id', 'user', 'title', 'duration', 'bpm', 'mood', 'genere',
                  'sample_file', 'wav')

    def create(self, validated_data):
        fileA = File(
            user=validated_data['user'],
            title=validated_data['title'],
            duration=validated_data['duration'],
            mood=validated_data['mood'],
            bpm=validated_data['bpm'],
            genere=validated_data['genere'],
            sample_file=validated_data['sample_file'],
            wav=validated_data['wav']
        )
        fileA.save()
        return fileA
