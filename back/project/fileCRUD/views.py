from django.shortcuts import render

from rest_framework import viewsets
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from rest_framework.permissions import IsAdminUser, IsAuthenticatedOrReadOnly
from rest_framework.authentication import TokenAuthentication

from .serializers import MoodSerializer, GenereSerializer, FileSerializer
from .models import File, Mood, Genere
from .permissions import (UpdateOwnProfile, IsAdminOrReadOnly,
                          IsadminIsproducer, ReadOnly)


# Create your views here.


class GenereViewSet(viewsets.ModelViewSet):
    """ Create Update Genere """
    serializer_class = GenereSerializer
    queryset = Genere.objects.all()
    authentication_classes = ()
    permission_classes = (IsAdminOrReadOnly,)


class MoodViewSet(viewsets.ModelViewSet):
    """ Create Update MooD """
    serializer_class = MoodSerializer
    queryset = Mood.objects.all()
    authentication_classes = ()
    permission_classes = (IsAdminOrReadOnly,)


class FileViewSet(viewsets.ModelViewSet):
    """ Create Update File """
    serializer_class = FileSerializer
    queryset = File.objects.all()
    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsadminIsproducer,)


class FileReadViewSet(viewsets.ModelViewSet):
    """ Only get Files """
    serializer_class = FileSerializer
    queryset = File.objects.all()
    authentication_classes = (TokenAuthentication,)
    permission_classes = (ReadOnly,)
