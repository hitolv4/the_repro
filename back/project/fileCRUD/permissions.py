from django.contrib.auth.models import AnonymousUser

from rest_framework import permissions


class UpdateOwnProfile(permissions.BasePermission):
    """allow user edit own profile."""

    def has_object_permission(self, request, view, obj):
        """check user is trying to edit """

        if request.method in permissions.SAFE_METHODS:
            return True

        return obj.id == request.user.id


class IsAdminOrReadOnly(permissions.BasePermission):
    """allow Admin Edit only"""

    def has_permission(self, request, view):
        """check admin is trying edit """

        if request.method in permissions.SAFE_METHODS:
            return True

        else:
            return request.user.is_staff


class IsadminIsproducer(permissions.BasePermission):
    """allow Admins and Producer Update Files"""

    def has_permission(self, request, view):
        """check is Admin or Producer is trying to edit"""
        if request.method in permissions.SAFE_METHODS:
            return True

        elif request.user.is_producer:
            return True

        else:
            return request.user.is_staff


class ReadOnly(permissions.BasePermission):
    """Only Get  files"""

    def has_permission(self, request, view):
        """check for Anonymus User"""
        if request.method in permissions.SAFE_METHODS:
            return True
