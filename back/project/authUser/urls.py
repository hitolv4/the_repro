from django.conf.urls import url, include

from rest_framework.routers import DefaultRouter

from .views import (UsersViewSet, UserProfileViewset,
                    ProducerProfileViewset, LoginViewSet, UserViewSet,
                    CountryViewSet, RegisterViewSet)

router = DefaultRouter()
router.register('user', UsersViewSet)
router.register('register', RegisterViewSet)
router.register('profile', UserProfileViewset)
router.register('producer_profile', ProducerProfileViewset)
router.register('login', LoginViewSet, base_name='login')
router.register('auth', UserViewSet, base_name='auth')
router.register('country', CountryViewSet, base_name='country')


urlpatterns = [
    url(r'', include(router.urls)),
]
