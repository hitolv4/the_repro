# Generated by Django 2.0.3 on 2018-04-16 18:57

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('authUser', '0001_initial'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='customuser',
            name='is_producer',
        ),
    ]
