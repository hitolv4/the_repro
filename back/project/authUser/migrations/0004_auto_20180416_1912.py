# Generated by Django 2.0.3 on 2018-04-16 19:12

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('authUser', '0003_auto_20180416_1906'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='customuser',
            name='profile',
        ),
        migrations.RemoveField(
            model_name='profile',
            name='is_producer',
        ),
        migrations.AddField(
            model_name='customuser',
            name='is_producer',
            field=models.BooleanField(default=False),
        ),
        migrations.AddField(
            model_name='profile',
            name='user',
            field=models.OneToOneField(default=1, limit_choices_to={'is_admin': False}, on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL),
            preserve_default=False,
        ),
    ]
