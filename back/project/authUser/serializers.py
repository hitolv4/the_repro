from rest_framework import serializers

from django_countries import Countries
from django_countries.serializers import CountryFieldMixin
from django_countries.serializer_fields import CountryField


from .models import CustomUser, Profile, ProducerProfile


class RegisterUserSerializer(serializers.ModelSerializer):
    """Serializer for User"""
    class Meta:
        model = CustomUser
        fields = ('id', 'username', 'email', 'first_name', 'last_name',
                  'password', 'user_type')
        extra_kwargs = {'password': {'write_only': True}}

    def create(self, validated_data):
        """Create and return new User"""
        user = CustomUser(
            username=validated_data['username'],
            email=validated_data['email'],
            first_name=validated_data['first_name'],
            last_name=validated_data['last_name'],
            user_type=validated_data['user_type'],
        )
        if user.user_type == 1:
            user.is_admin = True
        if user.user_type == 2:
            user.is_producer = True
        user.set_password(validated_data['password'])
        user.save()

        return user

    def update(self, instance, validated_data):
        """ Update User model"""

        instance.username = validated_data.get('username', instance.username)
        instance.email = validated_data.get('email', instance.email)
        instance.first_name = validated_data.get('first_name',
                                                 instance.first_name)
        instance.last_name = validated_data.get('last_name',
                                                instance.last_name)
        instance.user_type = validated_data.get('user_type',
                                                instance.user_type)
        if validated_data.get('password'):
            instance.set_password(validated_data.get('password',
                                                     instance.password))
        instance.save()
        return instance


class UserProfileSerializer(CountryFieldMixin, serializers.ModelSerializer):
    """Serializer for Profile"""
    class Meta:
        model = Profile
        fields = ('id', 'user', 'country', 'state', 'city', 'phoneNumber',
                  'whatsapp', 'gender', 'birthday', 'paypal_email',
                  'recovery_email')

    def create(self, validated_data):
        """Create and return New Profile"""
        profile = Profile(
            user=validated_data['user'],
            country=validated_data['country'],
            state=validated_data['state'],
            city=validated_data['city'],
            phoneNumber=validated_data['phoneNumber'],
            whatsapp=validated_data['whatsapp'],
            gender=validated_data['gender'],
            birthday=validated_data['birthday'],
            paypal_email=validated_data['paypal_email'],
            recovery_email=validated_data['recovery_email']
        )

        profile.save()

        return profile


class ProducerProfileSerializer(serializers.ModelSerializer):
    """Serializer for ProducerSerializer"""
    class Meta:
        model   = ProducerProfile
        fields  = ('id', 'user', 'bio', 'skill_sampling',
                  'skill_composition', 'skill_mixing',
                  'skill_mastering', 'skill_experience', 'my_tools',
                  'profile_pic')

        def create(self, validated_data):
            """Create and return New Producer Profile"""
            producer_profile = ProducerProfile(
                user=validated_data['user'],
                bio=validated_data['bio'],
                skill_sampling=validated_data['skill_sampling'],
                skill_composition=validated_data['skill_composition'],
                skill_mixing=validated_data['skill_mixing'],
                skill_mastering=validated_data['skill_matering'],
                skill_experience=validated_data['skill_experience'],
                my_tools=validated_data['my_tools'],
                profile_pic=validated_data['profile_pic'],
            )
            producer_profile.save()
            return producer_profile


class CountrySerializer(serializers.Serializer):
    country = CountryField()
