from django.shortcuts import render

from rest_framework import viewsets
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from rest_framework.parsers import JSONParser
from rest_framework.authentication import TokenAuthentication
from rest_framework.authtoken.serializers import AuthTokenSerializer
from rest_framework.authtoken.views import ObtainAuthToken
from rest_framework.permissions import IsAdminUser, IsAuthenticated
from rest_framework.authtoken.models import Token

from django_countries import countries

from json import JSONEncoder

from .serializers import (RegisterUserSerializer, UserProfileSerializer,
                          ProducerProfileSerializer, CountrySerializer)
from .models import CustomUser, Profile, ProducerProfile
from .permissions import UpdateOwnProfile, UpdateOwnData


# Create your views here.

class RegisterViewSet (viewsets.ModelViewSet):
    serializer_class = RegisterUserSerializer
    queryset = CustomUser.object.all()
    permission_classes = (UpdateOwnData,)

class UsersViewSet(viewsets.ModelViewSet):
    """Handles creating updating User"""
    serializer_class = RegisterUserSerializer
    queryset = CustomUser.object.all()
    authentication_classes = (TokenAuthentication,)
    permission_classes = (UpdateOwnData,)


class UserProfileViewset(viewsets.ModelViewSet):
    """Handles creating updating User Profiles"""
    serializer_class = UserProfileSerializer
    queryset = Profile.objects.all()
    authentication_classes = (TokenAuthentication,)
    permission_classes = (UpdateOwnProfile,)

    def retrieve(self, request, pk=None):
        user = request.user
        profile = Profile.objects.get(user=user.id)
        return Response({'id': profile.id, 'country': profile.country.name,
                         'state': profile.state, 'city': profile.city,
                         'phone_number': profile.phoneNumber,
                         'whatsapp': profile.whatsapp,
                         'gender': profile.gender,
                         'birthday': profile.birthday,
                         'paypal_email': profile.paypal_email,
                         'recovery_email': profile.recovery_email,
                         'user': profile.user_id})


class ProducerProfileViewset(viewsets.ModelViewSet):
    """Handles creating updating Producer Profiles"""
    serializer_class = ProducerProfileSerializer
    queryset = ProducerProfile.objects.all()
    authentication_classes = (TokenAuthentication,)
    permission_classes = (UpdateOwnProfile,)
    
    def retrieve(self, request, pk=None):
        user    = request.user
        profile = ProducerProfile.objects.get(user=user)
        return Response({
            'id'    :profile.id, 
            'bio'   :profile.bio})


class LoginViewSet(viewsets.ViewSet):
    """checks user and password and returns an auth token """
    serializer_class = AuthTokenSerializer

    def create(self, request):
        """Create a token"""
        tokegen = ObtainAuthToken().post(request)
        token = Token.objects.get(key=tokegen.data['token'])
        user = CustomUser.object.get(id=token.user_id)

        producer = ProducerProfile.objects.filter(user_id=token.user_id)
        if not producer and int(user.user_type) == 2:
            ProducerProfile(user_id=token.user_id).save()

        return Response({'token': token.key, 'id': token.user_id,
                        'user': user.username, 'type': user.user_type})


class UserViewSet(viewsets.ViewSet):
    """checks auth user"""
    authentication_classes = (TokenAuthentication,)
    serializer_class = RegisterUserSerializer

    def list(self, request):
        user = request.user

        return Response({'id': user.id, 'username': user.username,
                         'first_name': user.first_name,
                         'last_name': user.last_name,
                         'user_type': user.user_type})


class CountryViewSet(viewsets.ViewSet):
    """Return Countries"""
    def list(self, request):
        total = {}
        for code, name in list(countries):
            total.update({code: name})

        return Response(total)
