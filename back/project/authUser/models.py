from django.db import models
from django.contrib.auth.models import AbstractBaseUser
from django.contrib.auth.models import AnonymousUser as DjangoAnonymousUser
from django.contrib.auth.models import BaseUserManager
from django.conf import settings

from django_countries.fields import CountryField
# Create your models here.


class UserProfileManager(BaseUserManager):
    """ Managuer for  CustomUser"""

    def create_user(self, username, first_name, last_name, email, user_type,
                    password=None):
        """Creates a new user"""
        if not username:
            raise ValueError('User Must have an Username')

        email = self.normalize_email(email)
        user = self.model(username=username, email=email,
                          first_name=first_name, last_name=last_name,
                          user_type=user_type)
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_superuser(self, username, first_name, last_name, email,
                         user_type, password):
        """create Super User"""
        user = self.create_user(username, first_name,
                                last_name, email, user_type, password)
        user.is_admin = True

        user.save(using=self._db)
        return user


class CustomUser(AbstractBaseUser):
    """ Base User for Reg """
    USER_TYPE_CHOICES = (
        (1, 'admin'),
        (2, 'producer'),
        (3, 'regular'),
    )
    username = models.CharField(max_length=30, blank=False, unique=True)
    first_name = models.CharField(max_length=30, blank=False)
    last_name = models.CharField(max_length=30, blank=False)
    email = models.EmailField(
        verbose_name='email address', max_length=255, unique=True)
    user_type = models.PositiveIntegerField(
        choices=USER_TYPE_CHOICES, default=3)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    is_active = models.BooleanField(default=True)
    is_admin = models.BooleanField(default=False)
    is_producer = models.BooleanField(default=False)

    object = UserProfileManager()

    USERNAME_FIELD = 'username'
    EMAIL_FIELD = 'email'
    REQUIRED_FIELDS = [
        'first_name',
        'last_name',
        'email',
        'user_type'
    ]

    def __str__(self):
        """ get object to string """
        return self.username

    def has_perm(self, perm, obj=None):
        """Does the user have Specific permission?"""
        return True

    def has_module_perms(self, app_label):
        """Does the User have permitions to view the app 'app_label' ?"""
        return True

    def get_full_name(self):
        """ get full name of user """
        return self.first_name + " " + self.last_name

    def get_short_name(self):
        """ get short name of user """
        return self.first_name

    @property
    def is_staff(self):
        """ Is the User a menber of Staff?"""
        return self.is_admin


class Profile(models.Model):
    USER_GENDER = (
        ('m', 'male'),
        ('f', 'female'),
    )
    user = models.OneToOneField(settings.AUTH_USER_MODEL,
                                on_delete=models.CASCADE,
                                limit_choices_to={'is_admin': False})
    country = CountryField()
    state = models.CharField(max_length=100, blank=True)
    city = models.CharField(max_length=100, blank=True)
    phoneNumber = models.CharField(max_length=17, blank=True)
    whatsapp = models.CharField(max_length=17, blank=True)
    gender = models.CharField(max_length=1, choices=USER_GENDER)
    birthday = models.DateField()
    paypal_email = models.EmailField(
        verbose_name='paypal email', max_length=255, blank=True)
    recovery_email = models.EmailField(
        verbose_name='recovery email', max_length=255, blank=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)


class ProducerProfile(models.Model):
    user = models.OneToOneField(settings.AUTH_USER_MODEL,
                                on_delete=models.CASCADE,
                                limit_choices_to={'is_producer': True})
    bio = models.TextField(max_length=200, blank=True, default="")
    skill_sampling = models.IntegerField(blank=True, null=True)
    skill_composition = models.IntegerField(blank=True, null=True)
    skill_mixing = models.IntegerField(blank=True, null=True)
    skill_mastering = models.IntegerField(blank=True, null=True)
    skill_experience = models.IntegerField(blank=True, null=True)
    my_tools = models.TextField(max_length=255, blank=True, default="")
    profile_pic = models.ImageField(upload_to='images/authUser/profile')
